





import 'dart:io';

import 'package:flutter/material.dart';
import 'package:producttest/page/addproduct.dart';
import 'package:producttest/page/home.dart';
import 'package:producttest/page/showproduct.dart';

import '../database/databasehelp.dart';
import '../model/productmodel.dart';

class ListProduct extends StatefulWidget {
  const ListProduct({Key? key, required this.title}) : super(key: key);

  final String title;


  @override
  State<ListProduct> createState() => _ListProductState();
}

class _ListProductState extends State<ListProduct> {
  int? selectedId;


  @override
  void initState() {
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: NavigationDrawer(),
      appBar: AppBar(
        centerTitle: true,
        title: const Text('รายการสินค้า'),
        backgroundColor: Colors.brown,
      ),
      body: Column(
        children: [
          Padding(
            padding: EdgeInsets.fromLTRB(300, 620, 0, 0),
            child: IconButton(
              icon: const Icon(Icons.add_circle,size: 60,color: Colors.black),
              tooltip: 'Add Product',
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => AddProduct()),
                ).then((value) {
                  //getAllData();
                  setState(() {});
                });
              },
            ),
          ),
    Center(
        child: FutureBuilder<List<ProductModel>>(
        future: DatabaseHelp.instance.getProduct(),
        builder: (BuildContext context,
        AsyncSnapshot<List<ProductModel>> snapshot) {
          if (!snapshot.hasData) {
            return Center(child: Text('Loading...'));
          }
          return snapshot.data!.isEmpty
              ? Center(child: Text('No Profiles in List.'))
              : ListView(
                  children: snapshot.data!.map((grocery) {
            return Center(
                child: Card(
                color: selectedId == grocery.id
                ? Colors.white70
                    : Colors.white,
                child: ListTile(
                title: Text(
                '${grocery.productcode} ${grocery.productname}'),
          subtitle: Text(grocery.quantity ),
          leading: CircleAvatar(
          backgroundImage:
          FileImage(File(grocery.image))),
          trailing: Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
          // new IconButton(
          // padding: EdgeInsets.all(0),
          // icon: Icon(Icons.edit),
          // onPressed: () {
          // Navigator.push(
          // context,
          // MaterialPageRoute(
          // builder: (context) =>
          // EditProduct(grocery)),
          // ).then((value) {
          // setState(() {});
          // });
          // },
          // ),
    new IconButton(
    padding: EdgeInsets.all(0),
    icon: Icon(Icons.clear),
    onPressed: () {
    showDialog(
    context: context,
    builder: (BuildContext context) {
    return AlertDialog(
    title: new Text(
    "Do you want to delete this record?"),
    // content: new Text("Please Confirm"),
    actions: [
    new TextButton(
    onPressed: () {
    DatabaseHelp.instance
        .remove(grocery.id!);
    setState(() {
    Navigator.of(context).pop();
    });
    },
    child: new Text("Add"),
    ),
    Visibility(
    visible: true,
    child: new TextButton(
    onPressed: () {
    Navigator.of(context).pop();
    },
    child: new Text("Cancel"),
    ),
    ),
    ],
    );
    },
    );
    },
    ),
    ],
    ),
    onTap: () {
    var productid = grocery.id;
    Navigator.push(
    context,
    MaterialPageRoute(
    builder: (context) =>
    ShowProduct(id: productid)));
    setState(() {
    print(grocery.image);
    if (selectedId == null) {
    //firstname.text = grocery.firstname;
    selectedId = grocery.id;
    } else {
    // textController.text = '';
    selectedId = null;
    }
    });
    },
    onLongPress: () {
    setState(() {
    DatabaseHelp.instance.remove(grocery.id!);
    });
    },
    ),
    ),
    );
    }).toList(),
    );
    }),
    ),
    ]
    ),
    );
  }
}

