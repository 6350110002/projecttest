


import 'package:flutter/material.dart';
import 'package:producttest/page/home.dart';
import 'package:producttest/page/listproduct.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: ListProduct(title: 'ListProduct')
    );

  }
}


