
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:producttest/database/databasehelp.dart';
import 'package:producttest/model/productmodel.dart';

class ShowProduct extends StatelessWidget {
  final id;
  ShowProduct({Key? key, required this.id}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Detail Profile'),
      ),
      body: Center(
        child: FutureBuilder<List<ProductModel>>(
            future: DatabaseHelp.instance.getProduct(),
            builder: (BuildContext context,
                AsyncSnapshot<List<ProductModel>> snapshot) {
              if (!snapshot.hasData) {
                return Center(child: Text('Loading...'));
              }
              return snapshot.data!.isEmpty
                  ? Center(child: Text('No Groceries in List.'))
                  : ListView(
                children: snapshot.data!.map((product) {
                  return Center(
                    child: Column(
                      children: [
                        CircleAvatar(
                          backgroundImage: FileImage(File(product.image)),
                          radius: 100,
                        ),
                        Text('รหัสสินค้า: ${product.productcode}'),
                        Text('ชื่อสินค้า: ${product.productname}'),
                        Text('จำนวน: ${product. quantity}'),
                      ],
                    ),
                  );
                }).toList(),
              );
            }),
      ),
    );
  }
}